import wmi,time,requests

url = "http://192.168.0.102/Display"

while (True):
    time.sleep(3)
    data = wmi.WMI(namespace="root\OpenHardwareMonitor")        #query OpenHardwareMonitor program for sensor information
    temperature_infos = data.Sensor()
    answers = 0
    CPUtemp = None
    GPUtemp = None
    for sensor in temperature_infos:                            #parse OpenHardwareMonitor response
        if sensor.SensorType == 'Temperature':
            if sensor.Name == "CPU Package":
                CPUtemp = sensor.Value
                answers += 1
                if answers == 2:
                    break
            if sensor.Name == "GPU Core":
                GPUtemp = sensor.Value
                answers += 1
                if answers == 2:
                    break
    requests.post(url, {'CPU': CPUtemp,                         #don't need to store response unless want error handling
                        'GPU': GPUtemp})