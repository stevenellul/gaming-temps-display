#include "LED.h"
#include <ESP8266WiFi.h>
#include <WiFiClient.h>
#include <ESP8266mDNS.h>
#include <ESP8266WebServer.h>
#include "C:\Users\`\Dropbox\Arduino\Projects\wifiCredentials.h"                      //store wifi credentials outside of GIT repo

ESP8266WebServer server(80);                                                          //create a webserver object that listens for HTTP request on port 80

void handleDisplay();                                                                 //function prototypes
void handleNotFound();
void connectToWifi();
void startServer();

void setup() {
  // put your setup code here, to run once:
  Serial.begin(9600);                                                                 //start Serial Comms

  connectToWifi();

  startServer();

  delay(500);
  Serial.end();                                                                       //close serial comms

  FastLED.addLeds<LED_TYPE, LED_PIN, COLOR_ORDER>(rawLeds, NUM_LEDS);                 //set up array of leds to manipulate
  FastLED.setBrightness(30);
  mapLeds();                                                                          //function that maps led array to pointer array because leds are not physically in order
}

void loop() {
  // put your main code here, to run repeatedly:

  server.handleClient();                                                              //listen for HTTP requests from clients

}

void startServer() {
  server.on("/Display", HTTP_POST, handleDisplay);                                    //call the handleDisplay function when a POST request is made to URI /Display
  server.onNotFound(handleNotFound);                                                  //when a client requests an unknown URI call function handleNotFound

  server.begin();                                                                     //start server
  Serial.println("Server started");
  }

void connectToWifi() {
  WiFi.begin(ssid,password);                                                          //add wifi network
  Serial.print("Connecting to: ");
  Serial.print(ssid);
  while(WiFi.status() != WL_CONNECTED) {                                              //wait for wifi to connect
    delay(250);
    Serial.print(".");
    }
  
  Serial.print("\nConnected to: ");
  Serial.print(WiFi.SSID());  
  Serial.print("\nIP address is: ");
  Serial.print(WiFi.localIP());  

  if (MDNS.begin("esp8266")) {                                                        //enable resolving of hostnames over network
    Serial.println("\nmDNS responder started");
    }
    else {
      Serial.println("Error setting up mDNS responder");
      }
  }

void handleDisplay() {
  if ((server.hasArg("CPU")) && (server.hasArg("GPU"))){                                                          //if correct arguments provided update display
    String cpu = server.arg("CPU");
    String gpu = server.arg("GPU");
    clearDisplay();
    setDisplay(cpu[0],CRGB::Blue,0,0);
    setDisplay(cpu[1],CRGB::Blue,6,0);
    setDisplay(gpu[0],CRGB::Green,14,0);
    setDisplay(gpu[1],CRGB::Green,20,0);
    FastLED.show();                                                                   //tell display to update
    server.send(200, "text/plain", "Successfully updated display");
    return;
    }
  if (server.hasArg("OFF")) {
    clearDisplay();
    FastLED.show(); 
    server.send(200, "text/plain", "Successfully updated display");
    return;
    }

  server.send(204, "text/plain", "No Content");                                       //error message if missing arguments
  }
void handleNotFound() {
  server.send(404, "text/plain", "404: Not found");                                   //send HTTP status 404 (Not Found) when there's no handler for the URI in the request
  }
