#include <FastLED.h>                                              //library that handles LED strip
#define LED_TYPE    WS2812B
#define LED_PIN     14                                            //defines which pin LED strip is connected to
#define NUM_LEDS    127
#define COLOR_ORDER GRB
CRGB rawLeds[NUM_LEDS], *leds[25][5], *debugLed1, *debugLed2;     //CRGB is data type for LEDs

void mapLeds() {                                                  //this maps leds to ledLayout, instead of using straight strip, it has been cut and placed into a grid
  int temp;

  debugLed1 = &rawLeds[0];
  debugLed2 = &rawLeds[1];

  for (int i = 2; i < 27; i++) {
    leds[i - 2][0] = &rawLeds[i];
  }
  temp = 24;
  for (int i = 27; i < 52; i++) {
    leds[temp][1] = &rawLeds[i];
    temp--;
  }
  for (int i = 52; i < 77; i++) {
    leds[i - 52][2] = &rawLeds[i];
  }
  temp = 24;
  for (int i = 77; i < 102; i++) {
    leds[temp][3] = &rawLeds[i];
    temp--;
  }
  for (int i = 102; i < 127; i++) {
    leds[i - 102][4] = &rawLeds[i];
  }
}

void clearDisplay() {                                               //sets all leds to off (except debugLeds)
  for (int i = 2; i < 127; i++) {
    rawLeds[i] = CRGB::Black;
  }
}

void setDisplay(char number, CRGB colour, int x, int y) {       //leds x and y can be: [0][0], [6][0], [14][0] or [20][0]
//this function provides mapping of numbers and some symbols to the display, hardcoded because display is custom, thus no libraries
  switch (number) {
    case '0': {
        for (int i = 0; i < 5; i++) {
          if (i > 0 & i < 4) {
            *leds[x][y + i] = colour;
            *leds[x + 4][y + i] = colour;
          }
          *leds[x + i][y] = colour;
          *leds[x + i][y + 4] = colour;
        }
        break;
      }
    case '1': {
        for (int i = 0; i < 5; i++) {
          *leds[x + 4][y + i] = colour;
        }
        break;
      }
    case '2': {
        for (int i = 0; i < 5; i++) {
          if (i > 0 & i < 4) {
            *leds[x][y + i] = colour;
            *leds[x + 4][y + i] = colour;
          }
          *leds[x + i][y] = colour;
          *leds[x + i][y + 2] = colour;
          *leds[x + i][y + 4] = colour;
          *leds[x + 4][y + 3] = *leds[x][y + 1] = CRGB::Black;

        }
        break;
      }
    case '3': {
        for (int i = 0; i < 5; i++) {
          if (i > 0 & i < 4) {
            *leds[x][y + i] = colour;
            *leds[x + 4][y + i] = colour;
          }
          *leds[x + i][y] = colour;
          *leds[x + i][y + 2] = colour;
          *leds[x + i][y + 4] = colour;
          *leds[x][y + 3] = *leds[x][y + 1] = CRGB::Black;
        }
        break;
      }
    case '4': {
        for (int i = 0; i < 5; i++) {
          *leds[x + 4][y + i] = colour;
          *leds[x + i][y + 2] = colour;
        }
        *leds[x][y] = colour;
        *leds[x][y + 1] = colour;
        break;
      }
    case '5': {
        for (int i = 0; i < 5; i++) {
          if (i > 0 & i < 4) {
            *leds[x][y + i] = colour;
            *leds[x + 4][y + i] = colour;
          }
          *leds[x + i][y] = colour;
          *leds[x + i][y + 2] = colour;
          *leds[x + i][y + 4] = colour;
          *leds[x][y + 3] = *leds[x + 4][y + 1] = CRGB::Black;

        }
        break;
      }
    case '6': {
        for (int i = 0; i < 5; i++) {
          if (i > 0 & i < 4) {
            *leds[x][y + i] = colour;
            *leds[x + 4][y + i] = colour;
          }
          *leds[x + i][y] = colour;
          *leds[x + i][y + 2] = colour;
          *leds[x + i][y + 4] = colour;
          *leds[x + 4][y + 1] = CRGB::Black;
        }
        break;
      }
    case '7': {
        for (int i = 0; i < 5; i++) {
          *leds[x + 4][y + i] = colour;
          *leds[x + i][y] = colour;
        }
        break;
      }
    case '8': {
        for (int i = 0; i < 5; i++) {
          if (i > 0 & i < 4) {
            *leds[x][y + i] = colour;
            *leds[x + 4][y + i] = colour;
          }
          *leds[x + i][y] = colour;
          *leds[x + i][y + 2] = colour;
          *leds[x + i][y + 4] = colour;
        }
        break;
      }
    case '9': {
        for (int i = 0; i < 5; i++) {
          if (i > 0 & i < 4) {
            *leds[x][y + i] = colour;
            *leds[x + 4][y + i] = colour;
          }
          *leds[x + i][y] = colour;
          *leds[x + i][y + 2] = colour;
          *leds[x + i][y + 4] = colour;
          *leds[x][y + 3] = CRGB::Black;
        }
        break;
      }
    case ':': {
        *leds[12][1] = *leds[12][3] = colour;
        break;
      }

    case 'C': {
        for (int i = x; i < x + 2; i++) {
          *leds[i][0] = *leds[i][1] = colour;
        }
        for (int i = x + 4; i < x + 8; i++) {
          *leds[i][0] = *leds[i][4] = *leds[x + 3][1] = *leds[x + 3][2] = *leds[x + 3][3] = colour;
        }
        break;
      }

      return;
  }
}
