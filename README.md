# Gaming Temps Display Repository #

### What is this repository for? ###

This is the source code for a small project utilising an ESP8266 to drive a ws2812b led strip that has been arranged into a matrix to show live PC CPU and GPU temps

### What do the different files do? ###

1. LED.h contains functions and configurations to control LED strip
2. ESP8266_Project.ino contains code controlling micro-controller and handles connecting to wi-fi as well as runs a simple web server that receives POST requests and then updates display
3. Client script.py is PC application that sends CPU and GPU temps to micro-controller